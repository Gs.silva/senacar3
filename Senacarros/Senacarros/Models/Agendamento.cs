﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacarros.Models
{
    class Agendamento
    {

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }

        public Agendamento(string Nome, string Celular, string Email)
        {
            this.Nome = Nome;
            this.Celular = Celular;
            this.Email = Email;
        }

    }

    

}
