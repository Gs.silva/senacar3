﻿using Senacarros.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacarros.View
{
    public class Agendamento
    {
        public string Nome_Do_Modelo { get; set; }
    }

	[XamlCompilation(XamlCompilationOptions.Compile)]

	public partial class AgendamentoView : ContentPage
	{
		public AgendamentoView (Veiculo veiculo)
		{
			InitializeComponent();

            this.Title = veiculo.Nome_Do_Modelo;

		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Sucesso!","Agendamento com sucesso","ok");
        }

        private void ButtonFinalizar_Clicked(object sender, EventArgs e)
        {
            //Permissao();
        }

        //public async void Permissao()
        //{
        //    try
        //    {
        //        var status = await
        //        CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
        //        if (status != PermissionStatus.Granted)
        //        {
        //            if (await

        //            CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
        //            {
        //                await DisplayAlert("Permissão", "Precisamos da sua permissão para armazenar dados no dispositivo.", "OK");

        //            }
        //            var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage });

        //            status = results[Permission.Storage];
        //        }
        //        if (status == PermissionStatus.Granted)
        //        {
        //            //await DisplayAlert("Sucesso!", "Prossiga com a operação", "OK"); 
        //            FazerAgendamento();
        //        }
        //        else if (status != PermissionStatus.Unknown)
        //        {
        //            await DisplayAlert("Atenção", "Para utilizar os serviços de persistência de dados, aceite a permissão solicitada pelo dispositivo.", "OK");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //await DisplayAlert("Erro", "Erro no processo " + ex.Message, "OK");
        //    }


        //}

        private void ButtonAgendamento_Clicked(object sender, EventArgs e)
        {
            using (var conexao = DependencyService.Get<ISQLite>().Conexao())
            {
                AgendamentoDAO dAO = new AgendamentoDAO(conexao);

                dAO.Salvar(new Models.Agendamento("Modelo e Marca", "511234", "Joao@email.com"));

                DisplayAlert("Sucesso!", "Agendamento Realizado com sucesso", "Ok");


            }
        }

    }
}