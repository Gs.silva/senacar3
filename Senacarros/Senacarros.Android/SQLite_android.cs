﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Senacarros.Data;
using Senacarros.Droid;
using SQLite;



[assembly: Xamarin.Forms.Dependency(typeof(SQLite_android))]
namespace Senacarros.Droid
{         


        class SQLite_android : ISQLite
        {

            private const string arquivoDb = "senacar.db3";

            public SQLiteConnection Conexao()
            {

                var pathDb = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, arquivoDb);

                return new SQLite.SQLiteConnection(pathDb);


                //return new SQLite.SQLiteConnection("senacar.db3");
                //throw new NotImplementedException();
                

            }

        }



    
}